import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.bind(("0.0.0.0", 5600))

s.listen(10)

while True:
    conn, addr = s.accept()
    while True:
        try:
            conn.sendall(conn.recv(1024))
        except Exception as e:
            print(e)
            break