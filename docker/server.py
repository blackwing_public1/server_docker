from bwserver.server import BlackwingServer 
import os


configurations = [
        "hostname",
        "port",
        "microservices_folder",
        "server_type",
        "session_manager_ip",
        "session_manager_port",
        "session_expire" ,
        "expiration_mode",
        "expiration_time",
        "max_packet_size"
]

if not os.path.exists(os.environ.get("config_file_path".upper(), "/home/bwserver/server.yaml")):
    cmd = ""
    for key in configurations:
        key = key.upper()
        if key in os.environ:
            cmd += "--" + key.lower() + " " + os.environ[key] + " "
    print(cmd)
    os.system("python -m bwserver --create_config_file " + cmd)

server = BlackwingServer(
    os.environ.get("config_file_path".upper(), "/home/bwserver/server.yaml")
)

server.start()